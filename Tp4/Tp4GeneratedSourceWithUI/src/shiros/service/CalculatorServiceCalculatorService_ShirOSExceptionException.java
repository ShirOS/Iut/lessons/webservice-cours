
/**
 * CalculatorServiceCalculatorService_ShirOSExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.8  Built on : May 19, 2018 (07:06:11 BST)
 */

package shiros.service;

public class CalculatorServiceCalculatorService_ShirOSExceptionException extends java.lang.Exception{

    private static final long serialVersionUID = 1529323088862L;
    
    private shiros.service.CalculatorServiceCalculatorService_ShirOSException faultMessage;

    
        public CalculatorServiceCalculatorService_ShirOSExceptionException() {
            super("CalculatorServiceCalculatorService_ShirOSExceptionException");
        }

        public CalculatorServiceCalculatorService_ShirOSExceptionException(java.lang.String s) {
           super(s);
        }

        public CalculatorServiceCalculatorService_ShirOSExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public CalculatorServiceCalculatorService_ShirOSExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(shiros.service.CalculatorServiceCalculatorService_ShirOSException msg){
       faultMessage = msg;
    }
    
    public shiros.service.CalculatorServiceCalculatorService_ShirOSException getFaultMessage(){
       return faultMessage;
    }
}
    