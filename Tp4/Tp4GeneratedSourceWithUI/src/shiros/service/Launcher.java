package shiros.service;

public class Launcher {
    public static void main(String args[]) {
        try {
            CalculatorServiceStub stub = new CalculatorServiceStub();
            
            // Call GetVersion method
            Version version = new Version();
            VersionResponse responseV = stub.version(version);
            
            System.out.println(responseV.get_return());
            
            // Call Calcul method
            Calcul calcul = new Calcul();
            calcul.setArgs0(1);
            calcul.setArgs1(2);
            
            CalculResponse responseC = stub.calcul(calcul);

            System.out.println(responseC.get_return());
            
            // Call Calcul method
            Calcul calcul2 = new Calcul();
            calcul.setArgs0(1);
            calcul.setArgs1(0);
            
            CalculResponse responseC2 = stub.calcul(calcul2);
            
            System.out.println(responseC2.get_return());
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("\n\n\n");
        }
    }
}