package shiros;

import java.rmi.RemoteException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;

public class ServiceWebClient {
    private static final String URL = "http://localhost:8080/axis2/services/StringService_SOAPSession";
    private static final EndpointReference TARGET_EPR = new EndpointReference(ServiceWebClient.URL);

    public static OMElement concat(String chaine){
        OMFactory factory = OMAbstractFactory.getOMFactory();
        OMNamespace omNs = factory.createOMNamespace("http://service.shiros","xsd");
        OMElement method = factory.createOMElement("concat", omNs);
        OMElement value = factory.createOMElement("arg0", omNs);
        
        value.setText(chaine);
        method.addChild(value);
        
        return method;
    }
    
    public static void main(String[] args) {
        try {
            OMElement payload = ServiceWebClient.concat("Test");
            
            Options options = new Options();
            options.setTo(ServiceWebClient.TARGET_EPR);
            options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
            
            boolean soapsession = true;
            
            if (soapsession) {
                options.setManageSession(true);
                options.setAction("urn:concat");
                
                ServiceClient sender = new ServiceClient();                
            	sender.setOptions(options);
                sender.engageModule("addressing");
                
                OMElement result = sender.sendReceive(payload);
                String response = result.getFirstElement().getText();
                
                OMElement result2 = sender.sendReceive(payload);
                String response2 = result2.getFirstElement().getText();
                
                System.out.println(String.format("Result : %s", response));
                System.out.println(String.format("Result 2 : %s", response2));
            } else {                
                ServiceClient sender = new ServiceClient();
            	sender.setOptions(options);
                
                OMElement result = sender.sendReceive(payload);
                String response = result.getFirstElement().getText();
                
                System.out.println(String.format("Result : %s", response));            	
            }
        } catch (RemoteException e) {
            System.err.println(String.format("Remote Exception : %s", e.getMessage()));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
} 