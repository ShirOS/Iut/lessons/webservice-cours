package shiros;

public class Launcher {
    public static void main(String args[]) {
        try {
            StringServiceStub stub = new StringServiceStub();
            
            // Call Concate method
            shiros.StringServiceStub.Concat concat1 = new shiros.StringServiceStub.Concat();
            concat1.setArgs0("Test");
            shiros.StringServiceStub.ConcatResponse responseConcat1 = stub.concat(concat1);
            
            System.out.println(responseConcat1.get_return());
            
            // Call Concate method
            shiros.StringServiceStub.Concat concat2 = new shiros.StringServiceStub.Concat();
            concat2.setArgs0("Test 2");
            shiros.StringServiceStub.ConcatResponse responseConcat2 = stub.concat(concat2);
            
            System.out.println(responseConcat2.get_return());
            
            // Call Concate method
            shiros.StringServiceStub.ReadStr readStr = new shiros.StringServiceStub.ReadStr();
            shiros.StringServiceStub.ReadStrResponse responseRead1 = stub.readStr(readStr);
            
            System.out.println(responseRead1.get_return());

        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("\n\n\n");
        }
    }
}