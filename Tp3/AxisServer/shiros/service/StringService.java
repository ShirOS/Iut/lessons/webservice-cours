package shiros.service;

import java.rmi.RemoteException;

public class StringService {
	protected String concatResult = "";

	public String concat(String str) throws ShirOSException {
		if (str.equals("")) {
			throw new ShirOSException("Arg cannot be empty !");
		}

		
		if (concatResult.equals("")) { 
			this.concatResult = str;
		} else {
			this.concatResult = String.format("%s - %s", this.concatResult, str);
		}

		return this.concatResult;
	}

	public String readStr() throws ShirOSException {
		if (this.concatResult.equals("")) {
			throw new ShirOSException("You must be call at first the concat method.");
		}

		return this.concatResult;
	}

	public static class ShirOSException extends RemoteException {
		protected Throwable throwable;

		public ShirOSException(String message) {
				super(message);
		}

		public ShirOSException(String message, Throwable throwable) {
				super(message);
				this.throwable = throwable;
		}
	}
}