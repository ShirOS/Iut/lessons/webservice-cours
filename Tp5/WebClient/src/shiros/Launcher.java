package shiros;

public class Launcher {
    public static void main(String args[]) {
        try {
            ServStub stub = new ServStub();
            
            // Call Resolve Ip method
            ServStub.Donne donne = new ServStub.Donne();
            donne.setI(5);

            ServStub.DonneResponse response = stub.donne(donne);            
            System.out.println(String.format("Response : %s", response.get_return()));
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("\n\n\n");
        }
    }
}