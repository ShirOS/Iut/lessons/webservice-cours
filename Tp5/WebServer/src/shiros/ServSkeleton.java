/**
 * ServSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.2  Built on : May 02, 2016 (05:55:18 BST)
 */
package shiros;

/**
 *  ServSkeleton java skeleton for the axisService
 */
public class ServSkeleton {

    protected wtp.xsd.Donne number;

    /**
     * Auto generated method signature
     *
     * @param donne
     * @return donneResponse
     */
    public wtp.xsd.DonneResponse donne(wtp.xsd.Donne donne) {
        if (number == null) {
            this.number = new wtp.xsd.Donne();
            this.number.setI(0);
        }

        number.setI(number.getI() + donne.getI());

        wtp.xsd.DonneResponse response = new wtp.xsd.DonneResponse();
        response.set_return(this.number.getI());

        return response;
    }
}
