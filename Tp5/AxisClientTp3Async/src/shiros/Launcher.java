package shiros;

public class Launcher {
	public static void main(String args[]) {
		try {
			StringServiceStub stub = new StringServiceStub();

			// Call Concate method
			StringServiceStub.Concat concat1 = new StringServiceStub.Concat();
			concat1.setArgs0("Test");

			StringServiceStub.Concat concat2 = new StringServiceStub.Concat();
			concat2.setArgs0("Test 2");

			StringServiceStub.ReadStr readStr = new StringServiceStub.ReadStr();

			StringServiceCallbackHandler callback = new StringServiceCallbackHandler(){
				public void receiveResultreadStr(shiros.StringServiceStub.ReadStrResponse result) {
					System.out.println(String.format("Response (Read) : %s", result.get_return()));
				}

				public void receiveErrorreadStr(java.lang.Exception e) {
					System.out.println(String.format("Response Error (Read): %s", e.getMessage()));
				}
				
				public void receiveResultconcat(StringServiceStub.ConcatResponse result) {
					System.out.println(String.format("Response (Concat): %s", result.get_return()));
				}

				public void receiveErrorconcat(java.lang.Exception e) {
					System.out.println(String.format("Response Error (Concat): %s", e.getMessage()));
				}
			};

			stub.startconcat(concat1, callback);
			stub.startconcat(concat2, callback);
			stub.startreadStr(readStr, callback);
                        
            Thread.sleep(5000);
		} catch (Exception exception) {
			exception.printStackTrace();
			System.out.println("\n\n\n");
		}
	}
}