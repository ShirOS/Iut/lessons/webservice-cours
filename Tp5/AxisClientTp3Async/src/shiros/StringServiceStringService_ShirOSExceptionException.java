/**
 * StringServiceStringService_ShirOSExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.2  Built on : May 02, 2016 (05:55:18 BST)
 */
package shiros;

public class StringServiceStringService_ShirOSExceptionException extends java.lang.Exception {
    private static final long serialVersionUID = 1529499448333L;
    private shiros.StringServiceStub.StringServiceStringService_ShirOSException faultMessage;

    public StringServiceStringService_ShirOSExceptionException() {
        super("StringServiceStringService_ShirOSExceptionException");
    }

    public StringServiceStringService_ShirOSExceptionException(
        java.lang.String s) {
        super(s);
    }

    public StringServiceStringService_ShirOSExceptionException(
        java.lang.String s, java.lang.Throwable ex) {
        super(s, ex);
    }

    public StringServiceStringService_ShirOSExceptionException(
        java.lang.Throwable cause) {
        super(cause);
    }

    public void setFaultMessage(
        shiros.StringServiceStub.StringServiceStringService_ShirOSException msg) {
        faultMessage = msg;
    }

    public shiros.StringServiceStub.StringServiceStringService_ShirOSException getFaultMessage() {
        return faultMessage;
    }
}
