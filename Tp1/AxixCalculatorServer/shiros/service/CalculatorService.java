package shiros.service;

import java.rmi.RemoteException;

public class CalculatorService {
	protected static final String VERSION = "Version 1.0.0";

	public int calcul(int i, int j) throws ShirOSException {
		try {            
			return i/j;
		} catch (ArithmeticException arithmeticException) {
			throw new ShirOSException("The 2nd parameter can't be 0. The division operation can't be process with zero on denominator", arithmeticException);
		} catch (Throwable throwable) {
			throw new ShirOSException("Error during the proccessing of request", throwable);                
		}
	}

	public String version() {
		return CalculatorService.VERSION;
	}

	public static class ShirOSException extends RemoteException {
		protected Throwable throwable;

		public ShirOSException(String message) {
			super(message);
		}
		
		public ShirOSException(String message, Throwable throwable) {
			super(message);
			this.throwable = throwable;
		}
	}
}