
package shiros.client;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "HomeService", targetNamespace = "http://service.shiros/", wsdlLocation = "http://localhost:8080/Tp1/HomeService?wsdl")
public class HomeService_Service
    extends Service
{

    private final static URL HOMESERVICE_WSDL_LOCATION;
    private final static WebServiceException HOMESERVICE_EXCEPTION;
    private final static QName HOMESERVICE_QNAME = new QName("http://service.shiros/", "HomeService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/Tp1/HomeService?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        HOMESERVICE_WSDL_LOCATION = url;
        HOMESERVICE_EXCEPTION = e;
    }

    public HomeService_Service() {
        super(__getWsdlLocation(), HOMESERVICE_QNAME);
    }

    public HomeService_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), HOMESERVICE_QNAME, features);
    }

    public HomeService_Service(URL wsdlLocation) {
        super(wsdlLocation, HOMESERVICE_QNAME);
    }

    public HomeService_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, HOMESERVICE_QNAME, features);
    }

    public HomeService_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public HomeService_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns HomeService
     */
    @WebEndpoint(name = "HomeServicePort")
    public HomeService getHomeServicePort() {
        return super.getPort(new QName("http://service.shiros/", "HomeServicePort"), HomeService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns HomeService
     */
    @WebEndpoint(name = "HomeServicePort")
    public HomeService getHomeServicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://service.shiros/", "HomeServicePort"), HomeService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (HOMESERVICE_EXCEPTION!= null) {
            throw HOMESERVICE_EXCEPTION;
        }
        return HOMESERVICE_WSDL_LOCATION;
    }

}
