
package shiros.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the shiros.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConvertNumberToString_QNAME = new QName("http://service.shiros/", "convertNumberToString");
    private final static QName _DivisionWithSession_QNAME = new QName("http://service.shiros/", "divisionWithSession");
    private final static QName _ConvertNumberToStringResponse_QNAME = new QName("http://service.shiros/", "convertNumberToStringResponse");
    private final static QName _Hello_QNAME = new QName("http://service.shiros/", "hello");
    private final static QName _ShirOSException_QNAME = new QName("http://service.shiros/", "ShirOSException");
    private final static QName _HelloResponse_QNAME = new QName("http://service.shiros/", "helloResponse");
    private final static QName _Division_QNAME = new QName("http://service.shiros/", "division");
    private final static QName _DivisionResponse_QNAME = new QName("http://service.shiros/", "divisionResponse");
    private final static QName _DivisionWithSessionResponse_QNAME = new QName("http://service.shiros/", "divisionWithSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: shiros.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Division }
     * 
     */
    public Division createDivision() {
        return new Division();
    }

    /**
     * Create an instance of {@link DivisionWithSessionResponse }
     * 
     */
    public DivisionWithSessionResponse createDivisionWithSessionResponse() {
        return new DivisionWithSessionResponse();
    }

    /**
     * Create an instance of {@link DivisionResponse }
     * 
     */
    public DivisionResponse createDivisionResponse() {
        return new DivisionResponse();
    }

    /**
     * Create an instance of {@link ShirOSException }
     * 
     */
    public ShirOSException createShirOSException() {
        return new ShirOSException();
    }

    /**
     * Create an instance of {@link HelloResponse }
     * 
     */
    public HelloResponse createHelloResponse() {
        return new HelloResponse();
    }

    /**
     * Create an instance of {@link Hello }
     * 
     */
    public Hello createHello() {
        return new Hello();
    }

    /**
     * Create an instance of {@link DivisionWithSession }
     * 
     */
    public DivisionWithSession createDivisionWithSession() {
        return new DivisionWithSession();
    }

    /**
     * Create an instance of {@link ConvertNumberToString }
     * 
     */
    public ConvertNumberToString createConvertNumberToString() {
        return new ConvertNumberToString();
    }

    /**
     * Create an instance of {@link ConvertNumberToStringResponse }
     * 
     */
    public ConvertNumberToStringResponse createConvertNumberToStringResponse() {
        return new ConvertNumberToStringResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertNumberToString }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.shiros/", name = "convertNumberToString")
    public JAXBElement<ConvertNumberToString> createConvertNumberToString(ConvertNumberToString value) {
        return new JAXBElement<ConvertNumberToString>(_ConvertNumberToString_QNAME, ConvertNumberToString.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DivisionWithSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.shiros/", name = "divisionWithSession")
    public JAXBElement<DivisionWithSession> createDivisionWithSession(DivisionWithSession value) {
        return new JAXBElement<DivisionWithSession>(_DivisionWithSession_QNAME, DivisionWithSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertNumberToStringResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.shiros/", name = "convertNumberToStringResponse")
    public JAXBElement<ConvertNumberToStringResponse> createConvertNumberToStringResponse(ConvertNumberToStringResponse value) {
        return new JAXBElement<ConvertNumberToStringResponse>(_ConvertNumberToStringResponse_QNAME, ConvertNumberToStringResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Hello }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.shiros/", name = "hello")
    public JAXBElement<Hello> createHello(Hello value) {
        return new JAXBElement<Hello>(_Hello_QNAME, Hello.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShirOSException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.shiros/", name = "ShirOSException")
    public JAXBElement<ShirOSException> createShirOSException(ShirOSException value) {
        return new JAXBElement<ShirOSException>(_ShirOSException_QNAME, ShirOSException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HelloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.shiros/", name = "helloResponse")
    public JAXBElement<HelloResponse> createHelloResponse(HelloResponse value) {
        return new JAXBElement<HelloResponse>(_HelloResponse_QNAME, HelloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Division }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.shiros/", name = "division")
    public JAXBElement<Division> createDivision(Division value) {
        return new JAXBElement<Division>(_Division_QNAME, Division.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DivisionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.shiros/", name = "divisionResponse")
    public JAXBElement<DivisionResponse> createDivisionResponse(DivisionResponse value) {
        return new JAXBElement<DivisionResponse>(_DivisionResponse_QNAME, DivisionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DivisionWithSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.shiros/", name = "divisionWithSessionResponse")
    public JAXBElement<DivisionWithSessionResponse> createDivisionWithSessionResponse(DivisionWithSessionResponse value) {
        return new JAXBElement<DivisionWithSessionResponse>(_DivisionWithSessionResponse_QNAME, DivisionWithSessionResponse.class, null, value);
    }

}
