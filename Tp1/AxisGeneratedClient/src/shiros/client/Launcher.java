/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shiros.client;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alcaillot1
 */
public class Launcher {
    
    public static void main(String[] args) {
        int numberToConvert = 10;
        String convertResult = convertNumberToString(numberToConvert);
        
        System.out.println("----------------");
        System.out.println(String.format("Result de la convertion de (%d) : %s", numberToConvert, convertResult));
        
        try {
            int number1 = 10;
            int number2 = 2;
            int divisionResult = division(number1, number2);
            
            System.out.println("----------------");
            System.out.println(String.format("Result de la division de (%d et %d) : %d", number1, number2, divisionResult));
        } catch (ShirOSException_Exception ex) {
            Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("----------------");
            System.out.println(String.format("Exception : %s", ex.getMessage()));
        }
        
        try {
            int number1 = 10;
            int number2 = 0;
            int divisionResult = division(number1, number2);
            
            System.out.println("----------------");
            System.out.println(String.format("Result de la division de (%d et %d) : %d", number1, number2, divisionResult));
        } catch (ShirOSException_Exception ex) {
            System.out.println("----------------");
            System.out.println(String.format("Exception : %s", ex.getMessage()));
        }
        
        String name = "Shiroe_sama";
        String helloResult = hello(name);
        System.out.println("----------------");
        System.out.println(String.format("Result de hello de (%s) : %s", name, helloResult));
    }

    private static String convertNumberToString(int number) {
        shiros.client.HomeService_Service service = new shiros.client.HomeService_Service();
        shiros.client.HomeService port = service.getHomeServicePort();
        return port.convertNumberToString(number);
    }

    private static int division(int number1, int number2) throws ShirOSException_Exception {
        shiros.client.HomeService_Service service = new shiros.client.HomeService_Service();
        shiros.client.HomeService port = service.getHomeServicePort();
        return port.division(number1, number2);
    }

    private static String hello(java.lang.String name) {
        shiros.client.HomeService_Service service = new shiros.client.HomeService_Service();
        shiros.client.HomeService port = service.getHomeServicePort();
        return port.hello(name);
    }
}
