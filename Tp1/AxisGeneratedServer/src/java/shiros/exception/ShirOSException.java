/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shiros.exception;

import javax.xml.ws.WebFault;

/**
 *
 * @author alcaillot1
 */
@WebFault(name="ShirOSException")
public class ShirOSException extends Exception {
    protected Throwable throwable;
    
    public ShirOSException(String message) {
        super(message);
    }
    
    public ShirOSException(String message, Throwable throwable) {
        super(message);
        this.throwable = throwable;
    }
}
