package shiros;

public class Launcher {
    public static void main(String args[]) {
        try {
            CalculatorServiceStub stub = new CalculatorServiceStub();
            
            // Call GetVersion method
            shiros.CalculatorServiceStub.Version version = new shiros.CalculatorServiceStub.Version();
            shiros.CalculatorServiceStub.VersionResponse responseV = stub.version(version);
            
            System.out.println(responseV.get_return());
            
            // Call Calcul method
            shiros.CalculatorServiceStub.Calcul calcul = new shiros.CalculatorServiceStub.Calcul();
            calcul.setArgs0(1);
            calcul.setArgs1(2);
            
            shiros.CalculatorServiceStub.CalculResponse responseC = stub.calcul(calcul);

            System.out.println(responseC.get_return());
            
            // Call Calcul method
            shiros.CalculatorServiceStub.Calcul calcul2 = new shiros.CalculatorServiceStub.Calcul();
            calcul.setArgs0(1);
            calcul.setArgs1(0);
            
            shiros.CalculatorServiceStub.CalculResponse responseC2 = stub.calcul(calcul2);
            
            System.out.println(responseC2.get_return());
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("\n\n\n");
        }
    }
}