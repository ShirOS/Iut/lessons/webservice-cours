/**
 * CalculatorServiceCalculatorService_ShirOSExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.2  Built on : May 02, 2016 (05:55:18 BST)
 */
package shiros;

public class CalculatorServiceCalculatorService_ShirOSExceptionException
    extends java.lang.Exception {
    private static final long serialVersionUID = 1528286868048L;
    private shiros.CalculatorServiceStub.CalculatorServiceCalculatorService_ShirOSException faultMessage;

    public CalculatorServiceCalculatorService_ShirOSExceptionException() {
        super("CalculatorServiceCalculatorService_ShirOSExceptionException");
    }

    public CalculatorServiceCalculatorService_ShirOSExceptionException(
        java.lang.String s) {
        super(s);
    }

    public CalculatorServiceCalculatorService_ShirOSExceptionException(
        java.lang.String s, java.lang.Throwable ex) {
        super(s, ex);
    }

    public CalculatorServiceCalculatorService_ShirOSExceptionException(
        java.lang.Throwable cause) {
        super(cause);
    }

    public void setFaultMessage(
        shiros.CalculatorServiceStub.CalculatorServiceCalculatorService_ShirOSException msg) {
        faultMessage = msg;
    }

    public shiros.CalculatorServiceStub.CalculatorServiceCalculatorService_ShirOSException getFaultMessage() {
        return faultMessage;
    }
}
