package shiros;

public class Launcher {
    public static void main(String args[]) {
        try {
            VersionStub stub = new VersionStub();
            
            // Call GetVersion method
            VersionStub.GetVersion getVersion = new VersionStub.GetVersion();
            VersionStub.GetVersionResponse response = stub.getVersion(getVersion);
            
            System.out.println(response.get_return());
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("\n\n\n");
        }
    }
}